#!/bin/bash

mlserver start . &
sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/sites-available/default && nginx -g 'daemon off;' &
   
wait -n
  
exit $?