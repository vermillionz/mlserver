FROM python:3.10.4

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y nginx && rm -rf /var/lib/apt/lists/*

COPY default /etc/nginx/sites-available/default

WORKDIR /usr/src/app

COPY entrypoint.sh ./
RUN chmod 755 ./entrypoint.sh

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --upgrade "protobuf==3.20.1" # Caso haja problemas com versão do protobuf

COPY models ./
COPY settings.json ./

CMD ["./entrypoint.sh"]